from django.db import models


class Region(models.Model):
    region = models.CharField(max_length=100)
    num = models.FloatField(default=0)
    desc = models.TextField(blank=True)
    coordinates = models.TextField(blank=True)
    enable = models.BooleanField(default=True)

    def __unicode__(self):
        return u'%s' % self.region

    def get_num(self):
        return int(self.num)

    def get_desc(self):
        lines = self.desc.split('\r\n')
        desc = ' '.join(lines)
        return desc

from django.shortcuts import render

from decent.models import Region


def index(request):
    return render(
        request,
        'decent/index.html',
        {
            'regions': Region.objects.filter(enable=True),
        }
    )

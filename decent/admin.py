from django.contrib import admin
from decent.models import Region


class RegionAdmin(admin.ModelAdmin):
    exclude = ('coordinates',)
    # pass

admin.site.register(Region, RegionAdmin)

# coding: utf-8

from django import forms

from suit_ckeditor.widgets import CKEditorWidget

from models import Article


class ArticleAdminForm(forms.ModelForm):

    class Meta:
        model = Article
        exclude = []
        widgets = {
            'body_uk': CKEditorWidget(editor_options={'startupFocus': True}),
            'body_ru': CKEditorWidget(editor_options={'startupFocus': True}),
            'body_en': CKEditorWidget(editor_options={'startupFocus': True}),
            'short_body_uk': CKEditorWidget(editor_options={'startupFocus': True}),
            'short_body_ru': CKEditorWidget(editor_options={'startupFocus': True}),
            'short_body_en': CKEditorWidget(editor_options={'startupFocus': True}),
        }


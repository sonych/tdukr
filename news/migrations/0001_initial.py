# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('meta_title_uk', models.CharField(max_length=500, verbose_name='Meta title', blank=True)),
                ('meta_title_ru', models.CharField(max_length=500, null=True, verbose_name='Meta title', blank=True)),
                ('meta_title_en', models.CharField(max_length=500, null=True, verbose_name='Meta title', blank=True)),
                ('meta_description_uk', models.TextField(verbose_name='Meta description', blank=True)),
                ('meta_description_ru', models.TextField(null=True, verbose_name='Meta description', blank=True)),
                ('meta_description_en', models.TextField(null=True, verbose_name='Meta description', blank=True)),
                ('meta_keywords_uk', models.CharField(max_length=500, verbose_name='Meta keywords', blank=True)),
                ('meta_keywords_ru', models.CharField(max_length=500, null=True, verbose_name='Meta keywords', blank=True)),
                ('meta_keywords_en', models.CharField(max_length=500, null=True, verbose_name='Meta keywords', blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('publish_status', models.IntegerField(default=1, help_text='With Draft chosen, will only be shown in admin interface.', verbose_name='Status', choices=[(1, 'Draft'), (2, 'Published')])),
                ('publish_date', models.DateTimeField(help_text="With Published chosen, won't be shown until this time", null=True, verbose_name='Published from', blank=True)),
                ('expiry_date', models.DateTimeField(help_text="With Published chosen, won't be shown after this time", null=True, verbose_name='Expires on', blank=True)),
                ('title_uk', models.CharField(max_length=255, verbose_name='Title')),
                ('title_ru', models.CharField(max_length=255, null=True, verbose_name='Title', blank=True)),
                ('title_en', models.CharField(max_length=255, null=True, verbose_name='Title', blank=True)),
                ('body_uk', models.TextField(verbose_name='Body', blank=True)),
                ('body_ru', models.TextField(null=True, verbose_name='Body', blank=True)),
                ('body_en', models.TextField(null=True, verbose_name='Body', blank=True)),
                ('short_body_uk', models.TextField(null=True, verbose_name='Short content', blank=True)),
                ('short_body_ru', models.TextField(null=True, verbose_name='Short content', blank=True)),
                ('short_body_en', models.TextField(null=True, verbose_name='Short content', blank=True)),
                ('slug', models.SlugField(null=True, verbose_name='Slug', blank=True)),
                ('author', models.ForeignKey(verbose_name='Author', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ('-publish_date', 'created', 'id'),
                'verbose_name': 'News',
                'verbose_name_plural': 'News',
            },
            bases=(models.Model,),
        ),
    ]

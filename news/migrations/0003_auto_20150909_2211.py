# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_auto_20150709_2336'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='article',
            options={'ordering': ('-publish_date', 'created', 'id'), 'verbose_name': '\u041d\u043e\u0432\u0438\u043d\u0430', 'verbose_name_plural': '\u041d\u043e\u0432\u0438\u043d\u0430'},
        ),
        migrations.AlterField(
            model_name='article',
            name='author',
            field=models.ForeignKey(verbose_name='\u0410\u0432\u0442\u043e\u0440', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='article',
            name='expiry_date',
            field=models.DateTimeField(help_text='\u0417\u0456 \u0441\u0442\u0430\u0442\u0443\u0441\u043e\u043c \u041e\u043f\u0443\u0431\u043b\u0456\u043a\u043e\u0432\u0430\u043d\u043e \u043d\u0435 \u0431\u0443\u0434\u0435 \u0432\u0456\u0434\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u0438\u0441\u044c \u043f\u0456\u0441\u043b\u044f \u0446\u044c\u043e\u0433\u043e \u0447\u0430\u0441\u0443', null=True, verbose_name='\u0417\u0430\u0432\u0435\u0440\u0448\u0443\u0454\u0442\u044c\u0441\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='article',
            name='publish_date',
            field=models.DateTimeField(help_text='\u0417\u0456 \u0441\u0442\u0430\u0442\u0443\u0441\u043e\u043c \u041e\u043f\u0443\u0431\u043b\u0456\u043a\u043e\u0432\u0430\u043d\u043e \u043d\u0435 \u0431\u0443\u0434\u0435 \u0432\u0456\u0434\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u0438\u0441\u044c \u0434\u043e \u0446\u044c\u043e\u0433\u043e \u0447\u0430\u0441\u0443', null=True, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0456\u043a\u0443\u0432\u0430\u0442\u0438 \u0432\u0456\u0434', blank=True),
        ),
        migrations.AlterField(
            model_name='article',
            name='publish_status',
            field=models.IntegerField(default=1, help_text='\u0417\u0456 \u0441\u0442\u0430\u0442\u0443\u0441\u043e\u043c \u0427\u0435\u0440\u043d\u0435\u0442\u043a\u0430 \u043f\u0443\u0431\u043b\u0456\u043a\u0430\u0446\u0456\u0457 \u0431\u0443\u0434\u0443\u0442\u044c \u0432\u0438\u0434\u0438\u043c\u0456 \u043b\u0438\u0448\u0435 \u0432 \u0430\u0434\u043c\u0456\u043d\u0446\u0456', verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441', choices=[(1, '\u0427\u0435\u0440\u043d\u0435\u0442\u043a\u0430'), (2, '\u041e\u043f\u0443\u0431\u043b\u0456\u043a\u043e\u0432\u0430\u043d\u043e')]),
        ),
        migrations.AlterField(
            model_name='article',
            name='short_body_en',
            field=models.TextField(null=True, verbose_name='\u041a\u043e\u0440\u043e\u0442\u043a\u0438\u0439 \u043e\u043f\u0438\u0441', blank=True),
        ),
        migrations.AlterField(
            model_name='article',
            name='short_body_ru',
            field=models.TextField(null=True, verbose_name='\u041a\u043e\u0440\u043e\u0442\u043a\u0438\u0439 \u043e\u043f\u0438\u0441', blank=True),
        ),
        migrations.AlterField(
            model_name='article',
            name='short_body_uk',
            field=models.TextField(null=True, verbose_name='\u041a\u043e\u0440\u043e\u0442\u043a\u0438\u0439 \u043e\u043f\u0438\u0441', blank=True),
        ),
        migrations.AlterField(
            model_name='article',
            name='slug',
            field=models.SlugField(null=True, blank=True, unique=True, verbose_name='\u0421\u043b\u0430\u0433'),
        ),
        migrations.AlterField(
            model_name='article',
            name='title_en',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043a', blank=True),
        ),
        migrations.AlterField(
            model_name='article',
            name='title_ru',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043a', blank=True),
        ),
        migrations.AlterField(
            model_name='article',
            name='title_uk',
            field=models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043a'),
        ),
    ]

# coding: utf-8

from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse, resolve

from views import news_list, news_article


urlpatterns = patterns('',
    url(r'^$', news_list, name='news_list'),
    url(r'(?P<slug>[-\w]*)/$', news_article, name='news_article'),
)

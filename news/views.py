# coding: utf-8

from django.shortcuts import render, get_object_or_404

from models import Article


def news_list(request):
    template = 'news/news_list.html'

    articles = Article.objects.published()

    return render(request, template, {
        'articles': articles,
    })


def news_article(request, slug):
    template = 'news/news_article.html'

    article = get_object_or_404(Article, slug=slug)

    return render(request, template, {
        'article': article,
    })
# -*- coding: utf-8 -*-

import datetime

from django.db import models
from django.db.models import signals
from django.conf import settings
from django.utils.translation import ugettext as _

from core.models.transmeta import TransMeta
from core.models import MetaDataModel, TimeStampedModel, PublishedModel
from core.manager import PublishableManager


class Article(MetaDataModel, TimeStampedModel, PublishedModel):
    __metaclass__ = TransMeta

    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               verbose_name=_('Author'), blank=True, null=True)
    title = models.CharField(_(u'Title'), max_length=255)
    body = models.TextField(_(u'Body'), blank=True)
    short_body = models.TextField(_(u'Short content'), blank=True, null=True)
    slug = models.SlugField(_(u'Slug'), blank=True, null=True, unique=True)

    objects = PublishableManager()

    class Meta:
        verbose_name = _('News')
        verbose_name_plural = _('News')
        ordering = ('-publish_date', 'created', 'id')
        translate = ('title', 'body', 'short_body')

    def __unicode__(self):
        return u'{title}'.format(title=self.title)


def update_post_publish_status(sender, instance, **kwargs):
    if (instance.publish_status == Article.CONTENT_STATUS_PUBLISHED and
            not instance.publish_date):
        now = datetime.datetime.now()
        instance.publish_date = now

signals.pre_save.connect(update_post_publish_status, sender=Article)

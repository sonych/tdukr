# coding: utf-8

import datetime

from django.db import models
from django.db.models import signals
from django.conf import settings
from django.utils.translation import ugettext as _

from core.models.transmeta import TransMeta
from core.models import TimeStampedModel, MetaDataModel, PublishedModel
from core.manager import PublishableManager


class Post(MetaDataModel, TimeStampedModel, PublishedModel):
    __metaclass__ = TransMeta

    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               verbose_name=_('Author'), blank=True, null=True)
    title = models.CharField(_(u'Title'), max_length=255)
    image = models.ImageField(_(u'Image'), upload_to='upload/blogpost', blank=True, null=True)
    body = models.TextField(_(u'Content'), blank=True, null=True)
    short_body = models.TextField(_(u'Short content'), blank=True, null=True)
    slug = models.SlugField(_(u'Slug'), blank=True, null=True, unique=True)

    objects = PublishableManager()

    class Meta:
        verbose_name = _('Post')
        verbose_name_plural = _('Posts')
        ordering = ('-publish_date', 'created', 'id')
        translate = ('title', 'body', 'short_body')

    def __unicode__(self):
        return u'{author}: {title}'.format(author=self.author, title=self.title)


def update_post_publish_status(sender, instance, **kwargs):
    if instance.publish_status == Post.CONTENT_STATUS_PUBLISHED and not instance.publish_date:
        now = datetime.datetime.now()
        instance.publish_date = now

signals.pre_save.connect(update_post_publish_status, sender=Post)

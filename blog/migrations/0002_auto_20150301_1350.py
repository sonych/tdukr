# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='post',
            old_name='meta_description',
            new_name='meta_description_uk',
        ),
        migrations.RenameField(
            model_name='post',
            old_name='meta_keywords',
            new_name='meta_keywords_uk',
        ),
        migrations.RenameField(
            model_name='post',
            old_name='meta_title',
            new_name='meta_title_uk',
        ),
        migrations.AddField(
            model_name='post',
            name='meta_description_en',
            field=models.TextField(null=True, verbose_name='Meta description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='meta_description_ru',
            field=models.TextField(null=True, verbose_name='Meta description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='meta_keywords_en',
            field=models.CharField(max_length=500, null=True, verbose_name='Meta keywords', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='meta_keywords_ru',
            field=models.CharField(max_length=500, null=True, verbose_name='Meta keywords', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='meta_title_en',
            field=models.CharField(max_length=500, null=True, verbose_name='Meta title', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='meta_title_ru',
            field=models.CharField(max_length=500, null=True, verbose_name='Meta title', blank=True),
            preserve_default=True,
        ),
    ]

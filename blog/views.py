# coding: utf-8

from django.conf import settings
from django.shortcuts import render, get_object_or_404
from django.http import Http404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from core.models import User
from models import Post


def post_list(request):
    template = 'blog/post_list.html'

    bloggers = User.bloggers.active()

    posts = Post.objects.published()
    paginator = Paginator(posts, settings.PAGINATION_LIMIT)

    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)

    return render(request, template, {
        'posts': posts,
        'bloggers': bloggers,
    })


def post_item(request, slug):
    template = 'blog/post_item.html'

    bloggers = User.bloggers.active()
    post = get_object_or_404(Post, slug=slug)

    return render(request, template, {
        'post': post,
        'bloggers': bloggers,
    })


def blogger_page(request, username):
    template = 'blog/post_author.html'

    try:
        blogger = User.bloggers.get(username=username)
    except User.DoesNotExist:
        raise Http404

    bloggers = User.bloggers.active()

    posts = Post.objects.published().filter(author=blogger)

    return render(request, template, {
        'posts': posts,
        'blogger': blogger,
        'bloggers': bloggers,
    })

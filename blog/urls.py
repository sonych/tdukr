# coding: utf-8

from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse, resolve
from views import post_list, post_item, blogger_page


urlpatterns = patterns('',
    url(r'^$', post_list, name='post_list'),
    url(r'post/(?P<slug>[-\w]*)/$', post_item, name='post_item'),
    url(r'blogger/(?P<username>[\w]*)/$', blogger_page, name='blogger_name'),
)

# coding: utf-8

import datetime
from fabric.api import abort, local, run
from fabric.context_managers import lcd


# settings
config = {
    'db_name': 'test',
    'branch': 'new-design',
}


def hello():
    print 'Hello world'


def make_backup():
    d = datetime.datetime.now()
    dir_name = d.strftime('%Y_%m_%d__%H_%M')
    local('mkdir backup/%s' % dir_name)
    local('pg_dump -U postgres %s > backup/%s/%s.sql' % (config['db_name'], dir_name, config['db_name']))
    local('tar -zcf backup/%s/media.tar.gz media' % dir_name)
    local('cp -R tdukr backup/%s/tdukr' % dir_name)


def pull_src():
    with lcd('source/tdukr'):
        local('git pull origin %s' % config['branch'])


def copy_src():
    local('cp -R source/tdukr .')
    local('cp source/local_settings.py tdukr/tdukr/local_settings.py')
    local('cp source/wsgi.py tdukr/tdukr/wsgi.py')


def run_manage_cmd():
    with lcd('tdukr'):
        local('./manage.py migrate')
        local('./manage.py collectstatic --noinput')


def restart_uwsgi():
    local('service uwsgi restart')


def deploy():
    make_backup()
    pull_src()
    copy_src()
    run_manage_cmd()
    restart_uwsgi()









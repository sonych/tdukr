# coding: utf-8

from django.conf import settings
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from core.forms import JoinAsForm
from core.models import About, User, Partner
from news.models import Article


def home(request):
    template = 'home.html'
    articles = Article.objects.published()[:settings.NEWS_WIDGET_LIMIT]
    bloggers = User.bloggers.active()
    # partners = Partner.objects.filter(active=True)

    return render(request, template, {
        'articles': articles,
        'bloggers': bloggers,
        # 'partners': partners,
    })


def about(request):
    template = 'about.html'
    about = About.objects.all()[0]

    return render(request, template, {
        'about': about,
    })


@csrf_exempt
def join_as(request):
    template = 'join_as.html'

    form = JoinAsForm()

    if request.method == 'POST':
        form = JoinAsForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, template, {
                'form': False,
            })
        
    return render(request, template, {
        'form': form,
    })

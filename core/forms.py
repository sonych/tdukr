# coding: utf-8
from django import forms
from suit_ckeditor.widgets import CKEditorWidget

from models import About, Member


class AboutAdminForm(forms.ModelForm):

    class Meta:
        model = About
        exclude = []
        widgets = {
            'content_uk': CKEditorWidget(editor_options={'startupFocus': True}),
            'content_ru': CKEditorWidget(editor_options={'startupFocus': True}),
            'content_en': CKEditorWidget(editor_options={'startupFocus': True}),
        }


class JoinAsForm(forms.ModelForm):

    class Meta:
        model = Member
        fields = ['first_name', 'last_name', 'middle_name', 'phone', 'email',
                  # 'birthday',
                  'home', 'research_interests', 'research_interests',
                  'scientific_achievements', 'activity', 'accept_to_company',
                  'agree_policy']

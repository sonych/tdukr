# coding: utf-8

from django.conf import settings
from django.db import models
from django.contrib.auth.models import AbstractUser, Group, UserManager
from django.utils.translation import ugettext as _

from transmeta import TransMeta

__all__ = (
    'User',
)


class BloggerManager(UserManager):

    def all(self):
        group = Group.objects.get(name=settings.BLOGGER_GROUP)
        return super(BloggerManager, self).all().filter(groups=group)

    def active(self):
        return self.all().filter(is_active=True)


class User(AbstractUser):
    __metaclass__ = TransMeta

    firstname = models.CharField(_(u'First name'), max_length=30, blank=True)
    lastname = models.CharField(_(u'Last name'), max_length=30, blank=True)
    description = models.TextField(_(u'Description'), blank=True,
        help_text=_(u'Short description about you'))
    date_of_birthday = models.DateField(_(u'Date of birthday'),
        blank=True, null=True)
    # TODO: validation of photo size
    photo = models.ImageField(_(u'Photo'), blank=True, null=True,
        upload_to='upload/user/photo')

    bloggers = BloggerManager()

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')
        ordering = ('username', 'id')
        translate = ('firstname', 'lastname', 'description')

    def __unicode__(self):
        return self.username

    @property
    def full_name(self):
        return u'%s %s' % (self.firstname, self.lastname)

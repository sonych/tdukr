# coding: utf-8

from django.db import models
from django.utils.translation import ugettext as _


class Member(models.Model):
    first_name = models.CharField(_(u'First name'), max_length=127)
    last_name = models.CharField(_(u'Last name'), max_length=127)
    middle_name = models.CharField(_(u'Middle name'), max_length=127)
    phone = models.CharField(_(u'Phone'), max_length=12)
    email = models.EmailField(_(u'Email'))
    birthday = models.DateField(_(u'Birthday'), help_text=_(u'example "12.12.1990"'), blank=True, null=True)
    home = models.CharField(_(u'Home'), max_length=255)
    research_interests = models.TextField(_(u'Research interests'))
    scientific_achievements = models.TextField(_(u'Scientific achievements'))
    activity = models.TextField(_(u'An activity that you want to do'))
    accept_to_company = models.BooleanField(_(u'Accept to company'), default=False)
    agree_policy = models.BooleanField(_(u'Agree policy'), default=False)

    created = models.DateTimeField(_(u'Created'), auto_now_add=True)

    class Meta:
        verbose_name = _(u'Member')
        verbose_name_plural = _(u'Members')
        ordering = ('created', 'id')

    def __unicode__(self):
        return u'%s %s' % (self.first_name, self.last_name)

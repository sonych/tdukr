# coding: utf-8

from django.db import models
from django.utils.translation import ugettext as _

from transmeta import TransMeta


class About(models.Model):
    __metaclass__ = TransMeta

    content = models.TextField(_(u'About text'))

    class Meta:
        verbose_name = _(u'About')
        verbose_name_plural = _(u'About')
        translate = ('content',)

    def __unicode__(self):
        return u'About'

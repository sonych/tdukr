# coding: utf-8

from django.db import models
from django.utils.translation import ugettext as _

from .transmeta import TransMeta

__all__ = (
    'Partner',
)


class Partner(models.Model):
    __metaclass__ = TransMeta

    title = models.CharField(_(u'Title'), max_length=200)
    description = models.TextField(_(u'Description'), blank=True)
    image = models.ImageField(_(u'Image'), upload_to='upload/partners')
    active = models.BooleanField(_(u'Active'), default=False)

    class Meta:
        translate = ('title', 'description', 'image')

    def __unicode__(self):
        return u'%s' % self.title

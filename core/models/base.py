# coding: utf-8

from django.db import models
from django.utils.translation import ugettext as _

from core.models.transmeta import TransMeta

__all__ = (
    'MetaDataModel',
    'TimeStampedModel',
    'PublishedModel'
)


class MetaDataModel(models.Model):
    """
    Abstract model that provides meta data for content.
    """
    __metaclass__ = TransMeta

    meta_title = models.CharField(_(u'Meta title'), max_length=500, blank=True)
    meta_description = models.TextField(_('Meta description'), blank=True)
    meta_keywords = models.CharField(verbose_name=_('Meta keywords'), max_length=500, blank=True)

    class Meta:
        abstract = True
        translate = ('meta_title', 'meta_description', 'meta_keywords')

    def __unicode__(self):
        return self.meta_title


class TimeStampedModel(models.Model):
    """
    Provides created and updated timestamps on models.
    """
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    def __unicode__(self):
        return super(TimeStampedModel, self).__unicode__()


class PublishedModel(models.Model):
    CONTENT_STATUS_DRAFT = 1
    CONTENT_STATUS_PUBLISHED = 2
    CONTENT_STATUS_CHOICES = (
        (CONTENT_STATUS_DRAFT, _('Draft')),
        (CONTENT_STATUS_PUBLISHED, _('Published')),
    )

    publish_status = models.IntegerField(_('Status'),
        choices=CONTENT_STATUS_CHOICES, default=CONTENT_STATUS_DRAFT,
        help_text=_('With Draft chosen, will only be shown in admin interface.'))
    publish_date = models.DateTimeField(_('Published from'),
        help_text=_("With Published chosen, won't be shown until this time"),
        blank=True, null=True)
    expiry_date = models.DateTimeField(_('Expires on'),
        help_text=_("With Published chosen, won't be shown after this time"),
        blank=True, null=True)

    class Meta:
        abstract = True

    def __unicode__(self):
        return super(PublishedModel, self).__unicode__()

    @property
    def is_published(self):
        return self.status == self.CONTENT_STATUS_PUBLISHED

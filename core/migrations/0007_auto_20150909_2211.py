# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20150709_2326'),
    ]

    operations = [
        migrations.CreateModel(
            name='Partner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043a')),
                ('description', models.TextField(verbose_name='\u041e\u043f\u0438\u0441', blank=True)),
                ('image', models.ImageField(upload_to=b'upload/partners')),
                ('active', models.BooleanField(default=False)),
            ],
        ),
        migrations.AlterModelOptions(
            name='about',
            options={'verbose_name': '\u041f\u0440\u043e \u043d\u0430\u0441', 'verbose_name_plural': '\u041f\u0440\u043e \u043d\u0430\u0441'},
        ),
        migrations.AlterModelOptions(
            name='member',
            options={'ordering': ('created', 'id'), 'verbose_name': '\u0423\u0447\u0430\u0441\u043d\u0438\u043a', 'verbose_name_plural': '\u0423\u0447\u0430\u0441\u043d\u0438\u043a\u0438'},
        ),
        migrations.AlterModelOptions(
            name='user',
            options={'ordering': ('username', 'id'), 'verbose_name': '\u041a\u043e\u0440\u0438\u0447\u0442\u0443\u0432\u0430\u0447', 'verbose_name_plural': '\u041a\u043e\u0440\u0438\u0441\u0442\u0443\u0432\u0430\u0447\u0456'},
        ),
        migrations.AlterField(
            model_name='about',
            name='content_en',
            field=models.TextField(null=True, verbose_name='\u041f\u0440\u043e \u043d\u0430\u0441: \u043e\u043f\u0438\u0441', blank=True),
        ),
        migrations.AlterField(
            model_name='about',
            name='content_ru',
            field=models.TextField(null=True, verbose_name='\u041f\u0440\u043e \u043d\u0430\u0441: \u043e\u043f\u0438\u0441', blank=True),
        ),
        migrations.AlterField(
            model_name='about',
            name='content_uk',
            field=models.TextField(verbose_name='\u041f\u0440\u043e \u043d\u0430\u0441: \u043e\u043f\u0438\u0441'),
        ),
        migrations.AlterField(
            model_name='member',
            name='activity',
            field=models.TextField(verbose_name='\u042f\u043a\u043e\u044e \u0434\u0456\u044f\u043b\u044c\u043d\u0456\u0441\u0442\u044e \u0412\u0438 \u0431 \u0445\u043e\u0442\u0456\u043b\u0438 \u0437\u0430\u0439\u043c\u0430\u0442\u0438\u0441\u044f'),
        ),
        migrations.AlterField(
            model_name='member',
            name='birthday',
            field=models.DateField(help_text='\u043f\u0440\u0438\u043a\u043b\u0430\u0434 "12.12.1990"', verbose_name='\u0414\u0430\u0442\u0430 \u043d\u0430\u0440\u043e\u0434\u0436\u0435\u043d\u043d\u044f'),
        ),
        migrations.AlterField(
            model_name='member',
            name='created',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0421\u0442\u0432\u043e\u0440\u0435\u043d\u043e'),
        ),
        migrations.AlterField(
            model_name='member',
            name='first_name',
            field=models.CharField(max_length=127, verbose_name="\u0406\u043c'\u044f"),
        ),
        migrations.AlterField(
            model_name='member',
            name='home',
            field=models.CharField(max_length=255, verbose_name='\u0410\u0434\u0440\u0435\u0441\u0430'),
        ),
        migrations.AlterField(
            model_name='member',
            name='last_name',
            field=models.CharField(max_length=127, verbose_name='\u041f\u0440\u0456\u0437\u0432\u0438\u0449\u0435'),
        ),
        migrations.AlterField(
            model_name='member',
            name='middle_name',
            field=models.CharField(max_length=127, verbose_name='\u041f\u043e-\u0431\u0430\u0442\u044c\u043a\u043e\u0432\u0456'),
        ),
        migrations.AlterField(
            model_name='member',
            name='phone',
            field=models.CharField(max_length=12, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d'),
        ),
        migrations.AlterField(
            model_name='member',
            name='research_interests',
            field=models.TextField(verbose_name='\u041d\u0430\u0443\u043a\u043e\u0432\u0456 \u0456\u043d\u0442\u0435\u0440\u0435\u0441\u0438'),
        ),
        migrations.AlterField(
            model_name='member',
            name='scientific_achievements',
            field=models.TextField(verbose_name='\u041d\u0430\u0443\u043a\u043e\u0432\u0456 \u0434\u043e\u0441\u044f\u0433\u043d\u0435\u043d\u043d\u044f'),
        ),
        migrations.AlterField(
            model_name='user',
            name='date_of_birthday',
            field=models.DateField(null=True, verbose_name='\u0414\u0430\u0442\u0430 \u043d\u0430\u0440\u043e\u0434\u0436\u0435\u043d\u043d\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='description_en',
            field=models.TextField(help_text='\u041a\u043e\u0440\u043e\u0442\u043a\u0438\u0439 \u043e\u043f\u0438\u0441 \u043f\u0440\u043e \u0432\u0430\u0441', null=True, verbose_name='\u041e\u043f\u0438\u0441', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='description_ru',
            field=models.TextField(help_text='\u041a\u043e\u0440\u043e\u0442\u043a\u0438\u0439 \u043e\u043f\u0438\u0441 \u043f\u0440\u043e \u0432\u0430\u0441', null=True, verbose_name='\u041e\u043f\u0438\u0441', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='description_uk',
            field=models.TextField(help_text='\u041a\u043e\u0440\u043e\u0442\u043a\u0438\u0439 \u043e\u043f\u0438\u0441 \u043f\u0440\u043e \u0432\u0430\u0441', verbose_name='\u041e\u043f\u0438\u0441', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='firstname_en',
            field=models.CharField(max_length=30, null=True, verbose_name="\u0406\u043c'\u044f", blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='firstname_ru',
            field=models.CharField(max_length=30, null=True, verbose_name="\u0406\u043c'\u044f", blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='firstname_uk',
            field=models.CharField(max_length=30, verbose_name="\u0406\u043c'\u044f", blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='lastname_en',
            field=models.CharField(max_length=30, null=True, verbose_name='\u041f\u0440\u0456\u0437\u0432\u0438\u0449\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='lastname_ru',
            field=models.CharField(max_length=30, null=True, verbose_name='\u041f\u0440\u0456\u0437\u0432\u0438\u0449\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='lastname_uk',
            field=models.CharField(max_length=30, verbose_name='\u041f\u0440\u0456\u0437\u0432\u0438\u0449\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='photo',
            field=models.ImageField(upload_to=b'upload/user/photo', null=True, verbose_name='\u0424\u043e\u0442\u043e', blank=True),
        ),
    ]

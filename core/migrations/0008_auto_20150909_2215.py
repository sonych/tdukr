# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20150909_2211'),
    ]

    operations = [
        migrations.RenameField(
            model_name='partner',
            old_name='description',
            new_name='description_uk',
        ),
        migrations.RenameField(
            model_name='partner',
            old_name='image',
            new_name='image_uk',
        ),
        migrations.RenameField(
            model_name='partner',
            old_name='title',
            new_name='title_uk',
        ),
        migrations.AddField(
            model_name='partner',
            name='description_en',
            field=models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441', blank=True),
        ),
        migrations.AddField(
            model_name='partner',
            name='description_ru',
            field=models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441', blank=True),
        ),
        migrations.AddField(
            model_name='partner',
            name='image_en',
            field=models.ImageField(null=True, upload_to=b'upload/partners', blank=True),
        ),
        migrations.AddField(
            model_name='partner',
            name='image_ru',
            field=models.ImageField(null=True, upload_to=b'upload/partners', blank=True),
        ),
        migrations.AddField(
            model_name='partner',
            name='title_en',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043a', blank=True),
        ),
        migrations.AddField(
            model_name='partner',
            name='title_ru',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043a', blank=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_auto_20150909_2215'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partner',
            name='active',
            field=models.BooleanField(default=False, verbose_name='Active'),
        ),
        migrations.AlterField(
            model_name='partner',
            name='image_en',
            field=models.ImageField(upload_to=b'upload/partners', null=True, verbose_name='\u0417\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u043d\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='partner',
            name='image_ru',
            field=models.ImageField(upload_to=b'upload/partners', null=True, verbose_name='\u0417\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u043d\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='partner',
            name='image_uk',
            field=models.ImageField(upload_to=b'upload/partners', verbose_name='\u0417\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u043d\u044f'),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Member',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=127, verbose_name='First name')),
                ('last_name', models.CharField(max_length=127, verbose_name='Last name')),
                ('middle_name', models.CharField(max_length=127, verbose_name='Middle name')),
                ('phone', models.CharField(max_length=12, verbose_name='Phone')),
                ('email', models.EmailField(max_length=75, verbose_name='Email')),
                ('birthday', models.DateField(verbose_name='Birthday')),
                ('home', models.CharField(max_length=255, verbose_name='\u0414\u043e\u043c\u0456\u0432\u043a\u0430')),
                ('research_interests', models.TextField(verbose_name='Research interests')),
                ('scientific_achievements', models.TextField(verbose_name='Scientific achievements')),
                ('activity', models.TextField(verbose_name='An activity that you want to do')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
            ],
            options={
                'ordering': ('created', 'id'),
                'verbose_name': 'Member',
                'verbose_name_plural': 'Members',
            },
            bases=(models.Model,),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_member'),
    ]

    operations = [
        migrations.AddField(
            model_name='member',
            name='accept_to_company',
            field=models.BooleanField(default=False, verbose_name='Accept to company'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='member',
            name='agree_policy',
            field=models.BooleanField(default=False, verbose_name='Agree policy'),
            preserve_default=True,
        ),
    ]

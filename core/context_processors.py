# coding: utf-8

import datetime

from django.conf import settings

from core.models import User
from pages.models import Category


def settings_context(request):

    return {
        'HOSTNAME': settings.HOSTNAME,
        'LANGUAGES': settings.LANGUAGES,
    }


def active_bloggers(request):
    bloggers = User.bloggers.active()
    return {'bloggers': bloggers}


def active_page_categories(request):
    categories = Category.objects.published().filter(
        parent_category__isnull=True)

    return {'categories': categories}


def current_year(request):
    return {
        'current_year': datetime.datetime.now().year,
    }


def url_path_without_lang(request):
    return {
        'full_path_without_lang': request.get_full_path()[4:]
    }


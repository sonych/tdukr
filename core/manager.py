# coding: utf-8

from django.db.models import Manager, Q
from django.utils.timezone import now

from core.models import PublishedModel


class PublishableManager(Manager):
    """
    Provides filter for restricting items returned by status and publish date.
    """

    def published(self):
        return self.filter(
            Q(publish_date__lte=now()) | Q(publish_date__isnull=True),
            Q(expiry_date__gte=now()) | Q(expiry_date__isnull=True),
            Q(publish_status=PublishedModel.CONTENT_STATUS_PUBLISHED))

    def get_by_natural_key(self, slug):
        return self.get(slug=slug)



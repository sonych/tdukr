# coding: utf-8

from django.contrib import admin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext as _
from django.core.files.images import get_image_dimensions

from core.models.member import Member
from core.models import User, About, Partner
from forms import AboutAdminForm


class MyUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User

    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            User._default_manager.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])

    # def clean_photo(self):
    #     photo = self.cleaned_data.get('photo')
    #
    #     print 'OKOKOKOK'
    #
    #     # if not picture:
    #     #     raise forms.ValidationError("No image!")
    #     # else:
    #     #     w, h = get_image_dimensions(picture)
    #     #     if w != 100:
    #     #         raise forms.ValidationError("The image is %i pixel wide. It's supposed to be 100px" % w)
    #     #     if h != 200:
    #     #         raise forms.ValidationError("The image is %i pixel high. It's supposed to be 200px" % h)
    #     return photo


class UserAdmin(UserAdmin):
    add_form = MyUserCreationForm
    list_display = ('username', 'email', 'firstname', 'lastname',
                    'user_groups', 'is_staff', 'is_active')
    readonly_fields = ('last_login', 'date_joined')

    def user_groups(self, obj):
        groups = [unicode(g) for g in obj.groups.all()]
        return ', '.join(groups)
    user_groups.short_description = _(u'Group')

    def get_fieldsets(self, request, obj=None, *args, **kwargs):
        if obj is None:
            self.suit_form_tabs = None
            return super(UserAdmin, self).get_fieldsets(request, obj)

        fieldsets = [
            (None, {
                'fields': ('username', 'password', ('date_joined', 'last_login'))
            }),
            (None, {
                'fields': ('email', 'date_of_birthday', 'photo')
            }),
            (_(u'Ukrainian'), {
                'classes': ('suit-tab', 'suit-tab-uk',),
                'fields': ('firstname_uk', 'lastname_uk', 'description_uk')
            }),
            (_(u'Russian'), {
                'classes': ('suit-tab', 'suit-tab-ru',),
                'fields': ('firstname_ru', 'lastname_ru', 'description_ru')
            }),
            (_(u'English'), {
                'classes': ('suit-tab', 'suit-tab-en'),
                'fields': ('firstname_en', 'lastname_en', 'description_en')
            })
        ]

        self.suit_form_tabs = (('uk', _(u'Ukrainian')),
                               ('ru', _(u'Russian')),
                               ('en', _(u'English')))

        if request.user.is_superuser:
            fieldsets.append((None, {
                    # 'classes': ('collapse',),
                    'fields': ('is_active', 'is_staff', 'is_superuser', 'groups',
                               'user_permissions')
                })
            )

        return fieldsets

    def get_queryset(self, request):
        qs = super(UserAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(id=request.user.id)


admin.site.register(User, UserAdmin)


class MemberAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'email', 'phone', 'created')
    readonly_fields = ('first_name', 'last_name', 'middle_name', 'email',
                       'phone', 'home', 'research_interests', #'birthday',
                       'scientific_achievements', 'activity')
    fieldsets = [
            (_(u'Full name'), {
                'fields': ('first_name', 'last_name', 'middle_name')
            }),
            (_(u'Contacts'), {
                'fields': ('phone', 'email')
            }),
            (None, {
                'fields': ('birthday', 'home')
            }),
            (_(u'About'), {
                'fields': ('research_interests', 'scientific_achievements',
                           'activity')
            }),
        ]

    def has_add_permission(self, request):
        return False

admin.site.register(Member, MemberAdmin)


class AboutAdmin(admin.ModelAdmin):
    form = AboutAdminForm

    def get_fieldsets(self, request, obj=None, *args, **kwargs):
        fieldsets = [
            (_(u'Ukrainian'), {
                'classes': ('suit-tab', 'suit-tab-uk',),
                'fields': ('content_uk',)
            }),
            (_(u'Russian'), {
                'classes': ('suit-tab', 'suit-tab-ru',),
                'fields': ('content_ru',)
            }),
            (_(u'English'), {
                'classes': ('suit-tab', 'suit-tab-en',),
                'fields': ('content_en',)
            }),
        ]

        self.suit_form_tabs = (('uk', _(u'Ukrainian')),
                               ('ru', _(u'Russian')),
                               ('en', _(u'English')))

        return fieldsets


admin.site.register(About, AboutAdmin)


class PartnerAdmin(admin.ModelAdmin):

    def get_fieldsets(self, request, obj=None, *args, **kwargs):
        fieldsets = [
            (None, {
                'fields': ('active',)
            }),
            (_(u'Ukrainian'), {
                'classes': ('suit-tab', 'suit-tab-uk',),
                'fields': ('title_uk', 'description_uk', 'image_uk')
            }),
            (_(u'Russian'), {
                'classes': ('suit-tab', 'suit-tab-ru',),
                'fields': ('title_ru', 'description_ru', 'image_ru')
            }),
            (_(u'English'), {
                'classes': ('suit-tab', 'suit-tab-en',),
                'fields': ('title_en', 'description_en', 'image_en')
            }),
        ]

        self.suit_form_tabs = (('uk', _(u'Ukrainian')),
                               ('ru', _(u'Russian')),
                               ('en', _(u'English')))

        return fieldsets

# admin.site.register(Partner, PartnerAdmin)

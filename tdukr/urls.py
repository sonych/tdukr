# coding: utf-8

from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.conf import settings


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


urlpatterns += i18n_patterns('',
    url(r'^$', 'core.views.home', name='home'),
    url(r'^about/$', 'core.views.about', name='about'),
    url(r'^about/join-as/$', 'core.views.join_as', name='join_as'),
    url(r'^blog/', include('blog.urls', namespace='blog')),
    url(r'^pages/', include('pages.urls', namespace='pages')),
    url(r'^news/', include('news.urls', namespace='news')),


)

urlpatterns += patterns('',
    url(r'^decent/', 'decent.views.index', name='decent_index'),
)


if settings.DEBUG:
    urlpatterns += patterns('django.contrib.staticfiles.views',
        url(r'^static/(?P<path>.*)$', 'serve'),
    )

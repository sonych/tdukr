/*
    Django filer integration for CKEditor
*/

function getGETParam(paramname) {
    paramname = paramname.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");

    var regexbuild = "[\\?&]" + paramname + "=([^&#]*)";
    var regex = new RegExp(regexbuild);
    var results = regex.exec(window.location.href);

    if(results == null)
        return "";
    else
        return results[1];
}

function integrateFilerAndCKEditor() {
    if(getGETParam("CKEditor") !== "") {
        Suit.$("a").each(function () {
            var $this = Suit.$(this);
            var oldHref = $this.attr("href");
            if(typeof(oldHref) != "undefined") {
                var questionSignIndex = oldHref.indexOf("?");
                var newHref;
                if(questionSignIndex === -1) {
                    newHref = oldHref + "?CKEditor=" + getGETParam("CKEditor") +
                        "&CKEditorFuncNum=" + getGETParam("CKEditorFuncNum") +
                        "&langCode=" + getGETParam("langCode");
                } else {
                    newHref = oldHref + "&CKEditor=" + getGETParam("CKEditor") +
                        "&CKEditorFuncNum=" + getGETParam("CKEditorFuncNum") +
                        "&langCode=" + getGETParam("langCode");                
                }
                $this.attr("href", newHref);
            }
        });

        Suit.$("a.insertlinkButton").removeAttr("onclick");
        Suit.$("a.insertlinkButton").click(function () {
            var _funcName = parseInt(getGETParam("CKEditorFuncNum"));
            var _data_url = Suit.$(this).attr("data-url");
            window.opener.CKEDITOR.tools.callFunction(_funcName, _data_url);
            window.close();
            return false;
        });
    }
}


$(document).ready(function() {
    integrateFilerAndCKEditor();
})

﻿jQuery(document).ready(function ($) {
    // tab setup
    $('.tabs-panel2').not(':first').hide();
    $('ul.tabs2').each(function () {
        var current = $(this).find('li.active');
        if (current.length < 1) { $(this).find('li:first').addClass('active'); }
        current = $(this).find('li.active a').attr('href');
        $(current).show();
    });

    // tab click
    $(document).on('click', 'ul.tabs2 a[href^="#"]', function (e) {
        e.preventDefault();
        var tabs = $(this).parents('ul.tabs2').find('li');
        var tab_next = $(this).attr('href');
        var tab_current = tabs.filter('.active').find('a').attr('href');
        $(tab_current).hide();
        tabs.removeClass('active');
        $(this).parent().addClass('active');
        $(tab_next).fadeIn();
        return false;
    });
});
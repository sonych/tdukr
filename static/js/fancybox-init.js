﻿jQuery(document).ready(function ($) {
    //fansybox. see more on http://fancyapps.com/fancybox/
    $('.fancybox').fancybox({ //подключаем фансибокс
        openEffect: 'none',
        closeEffect: 'none',
        nextEffect: 'fade',
        prevEffect: 'fade'
    });

    $('a.video-caption').fancybox({//открываем youtube-видео в фансибокс окне
        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
            media: {}
        }
    });

    $('a.video-caption').each(function () {//создаем миниатюры для видео
        var link = $(this).attr('href');
        link = link.split('http://youtu.be/');
        var video_thumb = 'http://i1.ytimg.com/vi/' + link[1] + '/0.jpg';
        $(this).prepend('<img src="' + video_thumb + '" />');
    });
});
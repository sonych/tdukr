/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
    config.toolbarCanCollapse = true;
    config.tabSpaces = 4;
    config.language = 'ru';
    config.forcePasteAsPlainText = true;
    //filebrowserBrowseUrl : '/admin/filer/folder/';
    //filebrowserUploadUrl : '/admin/filer/folder/';

    config.filebrowserBrowseUrl = '/admin/filer/folder/?_popup=1';
    config.toolbarCanCollapse = true;
    config.tabSpaces = 4;
    config.contentsLanguage = 'ru';
    config.forcePasteAsPlainText = true;
};

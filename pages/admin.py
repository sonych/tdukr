# coding: utf-8

from django.contrib import admin
from django.utils.translation import ugettext as _

from pages.models import Page, Category
from forms import PageAdminForm


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'is_published')
    prepopulated_fields = {'slug': ('title_uk',)}

    def get_fieldsets(self, request, obj=None, *args, **kwargs):
        fieldsets = [
            (None, {
                'fields': ('slug', 'parent_category')
            }),
            (_(u'Ukrainian'), {
                'classes': ('suit-tab', 'suit-tab-uk',),
                'fields': ('title_uk',)
            }),
            (_(u'Russian'), {
                'classes': ('suit-tab', 'suit-tab-ru',),
                'fields': ('title_ru',)
            }),
            (_(u'English'), {
                'classes': ('suit-tab', 'suit-tab-en',),
                'fields': ('title_en',)
            }),
            (_(u'Published'), {
                'classes': ('collapse',),
                'fields': ('publish_status', 'publish_date', 'expiry_date')
            }),
        ]

        self.suit_form_tabs = (('uk', _(u'Ukrainian')),
                               ('ru', _(u'Russian')),
                               ('en', _(u'English')))

        return fieldsets

    def is_published(self, obj):
        return obj.publish_status == obj.CONTENT_STATUS_PUBLISHED
    is_published.boolean = True
    is_published.admin_order_field = 'publish_status'

admin.site.register(Category, CategoryAdmin)


class PageAdmin(admin.ModelAdmin):
    form = PageAdminForm
    list_display = ('title', 'author', 'created', 'is_published', 'category',
                    'publish_date', 'expiry_date')
    list_filter = ('author', 'publish_status', 'category')
    prepopulated_fields = {'slug': ('title_uk',)}

    def get_fieldsets(self, request, obj=None, *args, **kwargs):
        fieldsets = [
            (None, {
                'fields': ('slug', 'category')
            }),
            (_(u'Published'), {
                'classes': ('collapse',),
                'fields': ('publish_status', 'publish_date', 'expiry_date')
            }),
            (_(u'Ukrainian'), {
                'classes': ('suit-tab', 'suit-tab-uk',),
                'fields': ('title_uk', 'short_body_uk', 'body_uk')
            }),
            (_(u'SEO Ukrainian'), {
                'classes': ('collapse', 'suit-tab', 'suit-tab-uk',),
                'fields': ('meta_title_uk', 'meta_description_uk', 'meta_keywords_uk')
            }),
            (_(u'Russian'), {
                'classes': ('suit-tab', 'suit-tab-ru',),
                'fields': ('title_ru', 'short_body_ru', 'body_ru')
            }),
            (_(u'SEO Russian'), {
                'classes': ('collapse', 'suit-tab', 'suit-tab-ru',),
                'fields': ('meta_title_ru', 'meta_description_ru', 'meta_keywords_ru')
            }),
            (_(u'English'), {
                'classes': ('suit-tab', 'suit-tab-en',),
                'fields': ('title_en', 'short_body_en', 'body_en')
            }),
            (_(u'SEO English'), {
                'classes': ('collapse', 'suit-tab', 'suit-tab-en',),
                'fields': ('meta_title_en', 'meta_description_en', 'meta_keywords_en')
            }),
        ]

        self.suit_form_tabs = (('uk', _(u'Ukrainian')),
                               ('ru', _(u'Russian')),
                               ('en', _(u'English')))

        return fieldsets

    def save_model(self, request, obj, form, change):
        # set author of page
        if not obj.author:
            obj.author = request.user
        obj.save()

    def is_published(self, obj):
        return obj.publish_status == obj.CONTENT_STATUS_PUBLISHED
    is_published.boolean = True
    is_published.admin_order_field = 'publish_status'

admin.site.register(Page, PageAdmin)


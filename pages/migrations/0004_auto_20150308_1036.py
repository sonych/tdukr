# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0003_category'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='category',
            name='meta_description_en',
        ),
        migrations.RemoveField(
            model_name='category',
            name='meta_description_ru',
        ),
        migrations.RemoveField(
            model_name='category',
            name='meta_description_uk',
        ),
        migrations.RemoveField(
            model_name='category',
            name='meta_keywords_en',
        ),
        migrations.RemoveField(
            model_name='category',
            name='meta_keywords_ru',
        ),
        migrations.RemoveField(
            model_name='category',
            name='meta_keywords_uk',
        ),
        migrations.RemoveField(
            model_name='category',
            name='meta_title_en',
        ),
        migrations.RemoveField(
            model_name='category',
            name='meta_title_ru',
        ),
        migrations.RemoveField(
            model_name='category',
            name='meta_title_uk',
        ),
    ]

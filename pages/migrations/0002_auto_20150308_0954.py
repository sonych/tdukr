# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('pages', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='author',
            field=models.ForeignKey(verbose_name='Author', blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='page',
            name='short_body_en',
            field=models.TextField(null=True, verbose_name='Short content', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='page',
            name='short_body_ru',
            field=models.TextField(null=True, verbose_name='Short content', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='page',
            name='short_body_uk',
            field=models.TextField(null=True, verbose_name='Short content', blank=True),
            preserve_default=True,
        ),
    ]

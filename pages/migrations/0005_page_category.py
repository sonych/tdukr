# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0004_auto_20150308_1036'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='category',
            field=models.ForeignKey(verbose_name='Category', blank=True, to='pages.Category', null=True),
            preserve_default=True,
        ),
    ]

# coding: utf-8

from django import forms

from suit_ckeditor.widgets import CKEditorWidget

from pages.models import Page, Category


class PageAdminForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(PageAdminForm, self).__init__(*args, **kwargs)
        self.fields['category'].required = True

    class Meta:
        model = Page
        exclude = []
        widgets = {
            'body_uk': CKEditorWidget(editor_options={'startupFocus': True}),
            'body_ru': CKEditorWidget(editor_options={'startupFocus': True}),
            'body_en': CKEditorWidget(editor_options={'startupFocus': True}),
            'short_body_uk': CKEditorWidget(editor_options={'startupFocus': True}),
            'short_body_ru': CKEditorWidget(editor_options={'startupFocus': True}),
            'short_body_en': CKEditorWidget(editor_options={'startupFocus': True}),
        }


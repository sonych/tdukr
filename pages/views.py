# coding: utf-8

from django.shortcuts import render, get_object_or_404

from models import Category, Page


def pages_category(request, slug):
    template = 'pages/pages_list.html'

    category = get_object_or_404(Category, slug=slug)


    return render(request, template, {
        'category': category,
        'pages': category.page_set.published(),
    })


def pages_page(request, slug):
    template = 'pages/pages_item.html'

    page = get_object_or_404(Page, slug=slug)

    return render(request, template, {
        'page': page,
    })

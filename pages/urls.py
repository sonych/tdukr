# coding: utf-8

from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse, resolve
from views import pages_category, pages_page


urlpatterns = patterns('',
    # url(r'^$', post_list, name='post_list'),
    url(r'category/(?P<slug>[-\w]*)/$', pages_category, name='category'),
    url(r'page/(?P<slug>[-\w]*)/$', pages_page, name='page'),
)
